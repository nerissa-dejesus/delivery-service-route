# Delivery Service 

a service with a REST API where it will be possible to: 
● Retrieve the routes within a defined origin and destination points; 
● Manage routes (implemented the CRUD); 

### Tech
* [Laravel] - for the backend
* [MySql] - for the database
* [Postman] - for testing

[public repository][https://bitbucket.org/nerissa-dejesus/delivery-service-route/src/master/]

### Installation
1. Install [laravel](https://laravel.com/docs/7.x/installation) v7+ to run.
2. Clone the repo 
```
git clone git@bitbucket.org:nerissa-dejesus/delivery-service-route.git
```
3. Open Terminal->go to root directory, run
```sh
composer install
```
4. Copy .env.example to a new .env file, run
```sh
cp .env.example .env
```
5. Create mysql database schema
6. Open .env file, update DB_DATABASE and mysql credentials
7. Open Terminal->go to root directory, run
```sh
php artisan key:generate
```
8. Migration : it will create tables such as tbldeliver_service and user_account
Open Terminal->go to root directory, run
```sh
php artisan migrate
```
9. Seeder : it will populate the tbldeliver_service table of (origin, destination, time, cost) and user_account table of (username, password, email, is_admin)
Open Terminal->go to root directory, run
```sh
php artisan db:seed --class=DatabaseSeeder
```

### Testing

Open Terminal->go to root directory, run
```sh
php artisan serve
```

|1. RETRIEVE ROUTES | VIEW|
|-------------|------------|
|HTTP Request | GET |
|URL Request |http://127.0.0.1:8000/api/deliver-service-api|
|Params| |
|Key|origin|
|Key|destination|
|Authorization| |
|Type | Basic Auth|

USER ACCOUNTS : As default, for admin access
```
username : admin
password : admin
```
for not admin
```
username : user001
password : user001
```

|2. INSERT ROUTES | STORE|
|-------------|------------|
|HTTP Request | POST |
|URL Request |http://127.0.0.1:8000/api/deliver-service-api/add|
|Body (form-data) | |
|Key|origin|
|Key|destination|
|Key|time|
|Key|cost|
|Authorization| |
|Type | Basic Auth|

Follow USER ACCOUNTS in view, only user with admin access can Store Routes

|3. EDIT ROUTES | UPDATE|
|-------------|------------|
|HTTP Request | POST |
|URL Request |http://127.0.0.1:8000/api/deliver-service-api/edit|
|Body (form-data) | |
|Key|origin|
|Key|destination|
|Key|time|
|Key|cost|
|Authorization| |
|Type | Basic Auth|

Follow USER ACCOUNTS in view, only user with admin access can Update Routes

|4. DELETE ROUTES | REMOVE|
|-------------|------------|
|HTTP Request | POST |
|URL Request |http://127.0.0.1:8000/api/deliver-service-api/delete|
|Body (form-data) | |
|Key|origin|
|Key|destination|
|Authorization| |
|Type | Basic Auth|

Follow USER ACCOUNTS in view, only user with admin access can Remove Routes.

5. If want to try the admin permission, you may create your own account 

 | CREATE USER | ACCOUNT|
|-------------|------------|
|HTTP Request | POST |
|URL Request |http://127.0.0.1:8000/api/deliver-service-api/add-account|
|Body (form-data) | |
|Key|username|
|Key|password|
|Key|email|
|Key|isAdmin (1 or 0)|
|Authorization| |
|Type | Basic Auth|
