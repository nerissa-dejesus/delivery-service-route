<?php

use Illuminate\Database\Seeder;

class Deliver_ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbldeliver_service')->insert([
        [   'origin' => "A",
            'destination' => "C" ,
            'time' => 1,
            'cost' => 20
        ],
        [
            'origin' => "A",
            'destination' => "E" ,
            'time' => 30,
            'cost' => 5
        ],
        [
            'origin' => "A",
            'destination' => "H" ,
            'time' => 10,
            'cost' => 1
        ],
        [
            'origin' => "C",
            'destination' => "B" ,
            'time' => 1,
            'cost' => 12
        ],
        [
            'origin' => "E",
            'destination' => "D" ,
            'time' => 3,
            'cost' => 5
        ],
        [
            'origin' => "H",
            'destination' => "E" ,
            'time' => 30,
            'cost' => 1
        ],
        [
            'origin' => "D",
            'destination' => "F" ,
            'time' => 4,
            'cost' => 50
        ],
        [
            'origin' => "F",
            'destination' => "I" ,
            'time' => 45,
            'cost' => 50
        ],
        [
            'origin' => "F",
            'destination' => "G" ,
            'time' => 40,
            'cost' => 50
        ],
        [
            'origin' => "I",
            'destination' => "B" ,
            'time' => 65,
            'cost' => 5
        ],
        [
            'origin' => "G",
            'destination' => "B" ,
            'time' => 64,
            'cost' => 73
        ]
        ]);
    }
}
