<?php

use Illuminate\Database\Seeder;

class User_AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_account')->insert([
            [   'username' => "admin",
                'password' => bcrypt("admin") ,
                'email' => "admin@upteamco.com",
                'is_admin' => 1
            ],
            [
                'username' => "user001",
                'password' => bcrypt("user001") ,
                'email' => "user001@upteamco.com",
                'is_admin' => 0
            ]
        ]);
    }
}
