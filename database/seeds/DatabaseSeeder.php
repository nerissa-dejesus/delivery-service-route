<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Deliver_ServiceSeeder::class);
        $this->call(User_AccountSeeder::class);
    }
}
