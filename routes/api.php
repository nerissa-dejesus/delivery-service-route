<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/deliver-service-api', 'DeliverServiceController@index');
Route::post('/deliver-service-api/add','DeliverServiceController@insert');
Route::post('/deliver-service-api/edit','DeliverServiceController@update');
Route::post('/deliver-service-api/delete','DeliverServiceController@remove');
Route::post('/deliver-service-api/add-account','DeliverServiceController@insert_user');