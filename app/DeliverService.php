<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliverService extends Model
{
    //
    protected $table = 'tbldeliver_service';
    public $timestamps = true;
}
