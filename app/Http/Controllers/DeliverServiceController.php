<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliverService;
use Illuminate\Support\Facades\Auth;
use App\User;

class DeliverServiceController extends Controller
{
    //
    protected $listRoutes = array(); 
    protected $route = array();
    protected $deliver_service;
    protected $origin = "";

    public function index(Request $request)
    {
        // if(!Auth::user()->is_admin)
        // {
        //     return response()->json(['message'=>'Permission failed.']);
        // }
        $this->origin = $request->origin;
        $destination = $request->destination;
        $this->deliver_service = DeliverService::all();

        $this->path($this->origin,$destination,"");

        $bestTime = 0; $bestCost = 0; $bestRoute = "";

        $resultArray["routes"] = array();
        
        if(count($this->listRoutes) >0)
        {
            for($i=0;$i<(count($this->listRoutes));$i++)
            {
                $totalTime = 0; $totalCost = 0; $routePath = "";
                
                for($j=0;$j<(count($this->listRoutes[$i])-1);$j++)
                {               
                    $result = $this->deliver_service->where('origin',$this->listRoutes[$i][$j])->where('destination' ,$this->listRoutes[$i][$j+1]);
                    foreach($result as $row) 
                    {
                        $routePath.=$row["origin"]."-";
                        $totalTime +=$row["time"];
                        $totalCost +=$row["cost"];
                    }                
                }
                $routePath.=$this->listRoutes[$i][$j];
                array_push($resultArray["routes"],["route" => $routePath , "time" => $totalTime, "cost" => $totalCost]);
                if(($totalTime < $bestTime && $totalCost < $bestCost)|| ($bestTime ==0 && $bestCost==0))
                {
                    $bestRoute = $routePath;
                    $bestTime = $totalTime;
                    $bestCost = $totalCost;
                }
            }
        }
        if($bestRoute == "")
        {
            $resultArray["message"] = "No Available Route";
        }
        else
        {
            $resultArray["best_path"] = "The best path is : ". $bestRoute . " Time:" . $bestTime . " Cost:" . $bestCost;
        }

        return response()->json($resultArray);
        
    }
    function path($ori,$dest,$oldOri)
    {
        $answer = $this->deliver_service->where('origin',$ori);

        if(count($this->route) > 0)
        {
            if($this->route[count($this->route)-1]==$dest)
            {
                $removeIndex = (count($this->route)-1) - array_search($oldOri,$this->route);
                for($i=0;$i<$removeIndex;$i++)
                {
                    $this->route=(array_slice($this->route,0,(count($this->route)-1)));
                }
            }
        }
        array_push($this->route,$ori);
        if(!count($answer))
        {
            $removeIndex = (count($this->route)-1) - array_search($oldOri,$this->route);
                for($i=0;$i<=$removeIndex;$i++)
                {
                    $this->route=(array_slice($this->route,0,(count($this->route)-1)));
                }
        }
        foreach($answer as $row) 
        {                
            if($row->destination == $dest)
            {   
                array_push($this->route,$dest);
                array_push($this->listRoutes,$this->route);
                if($ori == $this->origin)
                {
                    array_pop($this->listRoutes);
                }
                else
                    break;                 
            }
            else
                $this->path($row->destination,$dest,$ori);              
        }
    }

    public function insert(Request $request)
    {
        if(!Auth::user()->is_admin)
        {
            return response()->json(['message'=>'Permission failed.']);
        }
        $error = 0;
        $allData = DeliverService::where('origin', $request->origin)->get();
        $isExistOD = $allData->where('origin', $request->origin)->where('destination', $request->destination);

        if(count($isExistOD) == 1)
        {
            $resultArray["message"] = "origin and destination already exists.";
            $error++;
        }
        if($error == 0 )
        {
            $addRoute = new DeliverService();
            $addRoute->origin = $request->origin;
            $addRoute->destination = $request->destination;
            $addRoute->time = $request->time;
            $addRoute->cost = $request->cost;
            $addRoute->save();

            $resultArray["data"]=$addRoute;
            $resultArray["message"] = "inserted";
        }
        return response()->json($resultArray);
    }

    public function update(Request $request)
    {
        if(!Auth::user()->is_admin)
        {
            return response()->json(['message'=>'Permission failed.']);
        }
        $updateRoute = DeliverService::where('origin', $request->origin)->where('destination', $request->destination)->first();

        if(!$updateRoute)
        {
            $resultArray["message"] = "origin and destination does not exists.";
        }
        else
        {
            $updateRoute->origin = $request->origin;
            $updateRoute->destination = $request->destination;
            $updateRoute->time = $request->time;
            $updateRoute->cost = $request->cost;
            $updateRoute->save();

            $resultArray["data"]=$updateRoute;
            $resultArray["message"] = "updated";
        }
        return response()->json($resultArray);
    }

    public function remove(Request $request)
    {
        if(!Auth::user()->is_admin)
        {
            return response()->json(['message'=>'Permission failed.']);
        }
        $updateRoute = DeliverService::where('origin', $request->origin)->where('destination', $request->destination)->first();

        if(!$updateRoute)
        {
            $resultArray["message"] = "origin and destination does not exists.";
        }
        else
        {
            $updateRoute->delete();

            $resultArray["data"]=$updateRoute;
            $resultArray["message"] = "deleted";
        }
        return response()->json($resultArray);
    }

    public function insert_user(Request $request)
    {
        if(!Auth::user()->is_admin)
        {
            return response()->json(['message'=>'Permission failed.']);
        }

            $addUser = new User();
            $addUser->username = $request->username;
            $addUser->password = bcrypt($request->password);
            $addUser->email = $request->email;
            $addUser->is_admin = $request->isAdmin;
            $addUser->save();

            $resultArray["data"]=$addUser;
            $resultArray["message"] = "account inserted";

        return response()->json($resultArray);
    }

}
